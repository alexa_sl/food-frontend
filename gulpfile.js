var os = require('os');
var fs = require('fs');
var path = require('path');
var gulp = require('gulp');
var wireDepStream = require('wiredep').stream;
var jsHintStylish = require('jshint-stylish');
var $ = require('gulp-load-plugins')();

var devConfigPath = './gulpfile-dev.js';
var devConfig = {};

if (fs.existsSync(devConfigPath)) {
  devConfig = require(devConfigPath)
}

var gulpArguments = process.argv.splice(2);
var ENV = process.env.NODE_ENV || 'dev';
var isDev = ENV === 'dev';
var configFilename = 'config.' + (isDev ? 'dev' : 'prod') + '.js';
var templateCacheModuleName = 'app.templates';
var server = {
  host: devConfig.host || 'localhost',
  port: devConfig.port || 8000
};
var isPublicServer = server.host === '0.0.0.0';
var browserApp = (function () {
  //also can be 'firefox'
  var platformBrowserApp;

  switch (os.platform()) {
    case 'linux':
      platformBrowserApp = 'google-chrome';
      break;
    case 'darwin':
      platformBrowserApp = 'open /Applications/Google\\ Chrome.app';
      break;
    case 'win32':
      platformBrowserApp = 'chrome';
      break;
    default:
      $.util.log($.util.colors.red('Unsupported dev platform'));
      process.exit();
      break;
  }

  return devConfig.browser || platformBrowserApp;
})();

var prefixesBrowsers = [
  'Android 2.3',
  'Android >= 4',
  'Chrome >= 20',
  'Firefox >= 24', // Firefox 24 is the latest ESR
  'Explorer >= 8',
  'iOS >= 6',
  'Opera >= 12',
  'Safari >= 6'
];
var folders = {
  src: 'src',
  app: path.join('src', 'app'),
  indexFile: path.join('src', 'index.html'),
  appAssets: path.join('src', 'assets'),
  appComponents: path.join('src', 'components'),
  dev: path.join('built', 'dev'),
  prod: path.join('built', 'prod')
};
var buildFolder = isDev && gulpArguments[0] !== 'build'
  ? folders.dev
  : folders.prod;
var plumberConfig = {
  errorHandler: function (err) {
    $.util.log($.util.colors.red(err.toString()));
    $.util.beep();
  }
};

/****************                ****************/
/****************   MAIN TASKS   ****************/
/****************                ****************/
gulp.task('default', ['clean'], function () {
  'use strict';

  isDev
    ? gulp.start('build-flow', 'open-browser', 'watchers')
    : gulp.start('build', 'open-browser');
});

/****************               ****************/
/****************   COMMON TASKS   ****************/
/****************               ****************/
gulp.task('build-flow', ['process-index', 'process-less', 'process-vendor-less', 'process-assets']);

gulp.task('clean', function () {
  'use strict';

  return gulp.src([
    path.join(buildFolder, '*'),
    path.join(buildFolder, 'js'),
    path.join(buildFolder, 'css'),
    path.join(buildFolder, 'images'),
    path.join(folders.src, 'index.temp.html')
  ], {read: false})
    .pipe($.plumber(plumberConfig))
    .pipe($.rimraf());
});

gulp.task('server', [isDev ? 'build-flow' : 'prod:full-flow'], function () {
  'use strict';

  var notification = 'Dev server was started on: http://' +
    (isPublicServer ? 'localhost' : server.host) + ':' + server.port;

  return gulp.src(buildFolder)
    .pipe($.plumber(plumberConfig))
    .pipe($.webserver({
      host: server.host,
      port: server.port,
      livereload: true,
      fallback: 'index.html'
    }))
    .pipe($.notify(notification))
});

gulp.task('open-browser', ['server'], function () {
  'use strict';

  if (gulpArguments.indexOf('--not-open') === -1) {
    gulp.src(buildFolder + '/index.html')
      .pipe($.open('', {
        url: 'http://' + (isPublicServer ? 'localhost' : server.host) + ':' + server.port,
        app: browserApp
      }));
  }
});

gulp.task('watchers', function () {
  'use strict';

  gulp.watch([
    path.join(folders.src, '**', '*.+(less|css)')
  ], ['process-less']);

  gulp.watch([
    path.join(folders.src, '**', 'vendor.less')
  ], ['process-vendor-less']);

  gulp.watch([
    path.join(folders.src, '**', '*.js')
    //,    '!' + path.join(folders.app, 'config', configFilename)
  ], [
    'process-app-js'
  ]);
  gulp.watch(path.join(folders.app, 'config', configFilename), ['process-app-config']);
  gulp.watch(folders.indexFile, ['process-index']);
  gulp.watch(path.join(folders.src, '**', '*.+(html|jade)'), ['process-app-templates']);
  //gulp.watch(path.join(folders.appAssets, 'images', '**', '*.*'), ['process-assets-images']);
  //gulp.watch(path.join(folders.src, 'favicon.ico'), ['process-assets-favicon']);
  gulp.watch(path.join(folders.appAssets, 'fonts', '**', '*.*'), ['process-assets-fonts']);
});

gulp.task('wiredep', function () {

  return gulp.src('src/index.html')
    .pipe(wiredep({
      directory: 'bower_components',
      exclude: [/bootstrap\.js/, /bootstrap\.css/, /bootstrap\.css/, /foundation\.css/]
    }))
    .pipe(gulp.dest('src'));
});


gulp.task('process-app-polyfills', function () {
  'use strict';

  return gulp.src(path.join(folders.app, '**'))
    .pipe($.plumber(plumberConfig))
    .pipe(gulp.dest(path.join(buildFolder, 'js')));
});

gulp.task('process-app-config', function () {
  'use strict';

  return gulp.src(path.join(folders.app, 'config', configFilename))
    .pipe($.plumber(plumberConfig))
    .pipe($.rename('config.js'))
    .pipe($.jshint())
    .pipe($.jshint.reporter(jsHintStylish))
    .pipe($.notify(function (file) {
      if (file.jshint.success) {
        return false;
      }

      var errors = file.jshint.results.map(function (data) {
        if (data.error) {
          return '(' + data.error.line + ':' + data.error.character + ') ' + data.error.reason;
        }
      }).join('\n');

      return file.relative + ' (' + file.jshint.results.length + ' errors)\n' + errors;
    }))
    .pipe(gulp.dest(path.join(buildFolder, 'js')));
});

gulp.task('process-app-templates', function () {
  'use strict';

  var filterHtml = $.filter('**/*.html');
  var filterJade = $.filter('**/*.jade');

  return gulp.src(path.join(folders.src, '**', '*.+(html|jade)'))
    .pipe($.plumber(plumberConfig))
    .pipe(filterJade)
    .pipe($.jade())
    .pipe(filterJade.restore())
    .pipe(filterHtml)
    .pipe($.angularHtmlify())
    .pipe($.cleanhtml())
    .pipe($.angularTemplatecache({
      standalone: true,
      module: templateCacheModuleName,
      root: '/views/'
    }))
    .pipe(gulp.dest(path.join(buildFolder, 'js')));
});


gulp.task('process-app-inject-js', function () {
  'use strict';

  return gulp.src(folders.indexFile)
    .pipe($.inject(gulp.src(path.join(folders.src, '**', '*.js')).pipe($.angularFilesort()), {
      ignorePath: 'src'
    }))
    .pipe($.rename({
      basename: 'index.temp'
    }))
    .pipe(gulp.dest(folders.src));
});

gulp.task('process-app-js', ['process-app-inject-js'], function () {
  'use strict';

  return gulp.src(path.join(folders.src, 'index.temp.html'))
    .pipe($.plumber(plumberConfig))
    .pipe($.useref.assets())
    .pipe($.filter(function (file) {
      return /app\.js$/.test(file.path);
    }))
    .pipe($.size({title: 'Original js sources size: '}))
    .pipe($.ngAnnotate())
    //.pipe($.stripDebug())
    //.pipe($.uglifyjs())
    .pipe($.size({title: 'Compressed js sources size: '}))
    .pipe(gulp.dest(buildFolder));
});

gulp.task('process-vendors-js', function () {
  'use strict';

  return gulp.src(folders.indexFile)
    .pipe($.plumber(plumberConfig))
    .pipe(wireDepStream())
    .pipe($.useref.assets())
    .pipe($.filter(function (file) {
      return /vendors\.js$/.test(file.path);
    }))
    .pipe($.size({title: 'Original vendors size: '}))
    .pipe($.uglifyjs())
    .pipe($.size({title: 'Compressed vendors size: '}))
    .pipe(gulp.dest(buildFolder));
});

gulp.task('process-modernizr-js', function () {
  'use strict';

  return gulp.src(folders.indexFile)
    .pipe($.plumber(plumberConfig))
    .pipe(wireDepStream())
    .pipe($.useref.assets())
    .pipe($.filter(function (file) {
      return /modernizr\.js$/.test(file.path);
    }))
    .pipe($.size({title: 'Original modernizr size: '}))
    .pipe($.uglifyjs())
    .pipe($.size({title: 'Compressed modernizr size: '}))
    .pipe(gulp.dest(buildFolder));
});

gulp.task('process-index', [
  'process-app-config',
  'process-app-templates',
  'process-vendors-js',
  'process-app-js',
  'process-modernizr-js'
], function () {
  'use strict';

  return gulp.src(folders.indexFile)
    .pipe($.plumber(plumberConfig))
    .pipe($.useref())
    .pipe($.angularHtmlify())
    //.pipe($.cleanhtml())
    .pipe(gulp.dest(buildFolder));
});

gulp.task('process-less', function () {
  'use strict';

  return gulp.src(path.join(folders.app, 'build.less'))
    .pipe($.plumber(plumberConfig))
    .pipe($.less())
    .pipe($.cssimport())
    .pipe($.size({title: 'Original app styles size: '}))
    .pipe($.autoprefixer(prefixesBrowsers))
    .pipe($.csso())
    .pipe($.rename({
      basename: 'app'
    }))
    .pipe($.size({title: 'Compressed app styles size: '}))
    .pipe(gulp.dest(path.join(buildFolder, 'css')));
});

gulp.task('process-vendor-less', function () {
  'use strict';

  return gulp.src(path.join(folders.app, 'vendor.less'))
    .pipe($.plumber(plumberConfig))
    .pipe($.less())
    .pipe($.cssimport())
    .pipe($.size({title: 'Original styles vendor size: '}))
    .pipe($.autoprefixer(prefixesBrowsers))
    .pipe($.csso())
    .pipe($.rename({
      basename: 'vendor'
    }))
    .pipe($.size({title: 'Compressed styles vendor size: '}))
    .pipe(gulp.dest(path.join(buildFolder, 'css')));
});

gulp.task('process-assets-images', function () {
  'use strict';

  return gulp.src(path.join(folders.appAssets, 'images', '**', '*.*'))
    .pipe(gulp.dest(path.join(buildFolder, 'images')));
});

gulp.task('process-assets-favicon', function () {
  'use strict';

  return gulp.src(path.join(folders.appAssets, 'favicon', '*.*'))
    .pipe(gulp.dest(buildFolder));
});

gulp.task('process-assets-fonts', function () {
  'use strict';

  return gulp.src(path.join(folders.appAssets, 'fonts', '**', '*.*'))
    .pipe(gulp.dest(path.join(buildFolder, 'fonts')));
});

gulp.task('process-assets-before-js', function () {
  'use strict';

  return gulp.src(path.join(folders.appAssets, 'beforeJs', '**', '*.*'))
    .pipe(gulp.dest(path.join(buildFolder, 'js')));
});

gulp.task('process-assets', [
  'process-assets-images',
  'process-assets-favicon',
  'process-assets-fonts',
  'process-assets-before-js'
]);
