'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var os = require('os');
var path = require('path');
var gulpArguments = process.argv.splice(2);

var devServer = {
  host: 'localhost',
  port: 8000
};
var browserApp = (function () {
  //also can be 'firefox'
  var platformBrowserApp;

  switch (os.platform()) {
    case 'linux':
      platformBrowserApp = 'google-chrome';
      break;
    case 'darwin':
      platformBrowserApp = 'open /Applications/Google\\ Chrome.app';
      break;
    case 'win32':
      platformBrowserApp = 'chrome';
      break;
    default:
      $.util.log($.util.colors.red('Unsupported dev platform'));
      process.exit();
      break;
  }

  return platformBrowserApp;
})();
var folders = {
  app: 'scripts',
  src: 'src',
  config: path.join('scripts', 'config'),
  styles: 'styles',
  indexFile: 'index.html',
  maintenanceFile: 'maintenance.html',
  appViews: 'views',
  dev: path.join('built', 'dev'),
  prod: path.join('built', 'prod')
};
var buildFolder = '.tmp/app';

var plumberConfig = {
  errorHandler: function (err) {
    $.util.log($.util.colors.red(err.toString()));
    $.util.beep();
  }
};

gulp.task('server', ['index'],  function () {
  'use strict';

  return gulp.src(buildFolder)
    .pipe($.plumber(plumberConfig))
    .pipe($.webserver({
      host: devServer.host,
      port: devServer.port,
      livereload: true,
      fallback: 'index.html'
    }))
    .pipe($.notify('Dev server was started on: http://' + devServer.host + ':' + devServer.port))
});

gulp.task('open-browser', ['server'], function () {
  'use strict';

  if (gulpArguments.indexOf('--not-open') === -1) {
    gulp.src(buildFolder + '/questions.page.html')
      .pipe($.open('', {
        url: 'http://' + devServer.host + ':' + devServer.port,
        app: browserApp
      }));
  }
});

gulp.task('index', ['watch'], function() {
  'use strict';

  return gulp.src(folders.src + '/questions.page.html')
    .pipe(gulp.dest(buildFolder));
});

//var util = require('util');
//
//var browserSync = require('browser-sync');
//
//var middleware = require('./proxy');
////var middleware = function (connect, options) {
////  var optBase = (typeof options.base === 'string') ? [options.base] : options.base;
////  return [require('connect-modrewrite')(['!(\\..+)$ / [L]'])].concat(
////    optBase.map(function(path){ return connect.static(path); }));
////};
//
//function browserSyncInit(baseDir, files, browser) {
//  browser = browser === undefined ? 'default' : browser;
//
//  var routes = null;
//  if(baseDir === 'src' || (util.isArray(baseDir) && baseDir.indexOf('src') !== -1)) {
//    routes = {
//      '/bower_components': 'bower_components'
//    };
//  }
//
//  browserSync.instance = browserSync.init(files, {
//    startPath: '/',
//    server: {
//      baseDir: baseDir,
//      //middleware: middleware,
//      routes: routes,
//      options: {
//        open: false,
//        middleware: function (connect) {
//          return [
//            modRewrite(['^[^\\.]*$ /questions.page.html [L]']),
//            connect.static('.tmp'),
//            connect().use(
//              '/bower_components',
//              connect.static('./bower_components')
//            ),
//            connect.static(appConfig.app)
//          ];
//        }
//      }
//    },
//    browser: browser
//  });
//
//}
//
gulp.task('serve', ['open-browser'], function () {
//  browserSyncInit([
//    '.tmp',
//    'src'
//  ], [
//    '.tmp/{app,components}/**/*.css',
//    'src/{app,components}/**/*.js',
//    'src/assets/images/**/*',
//    '.tmp/*.html',
//    '.tmp/{app,components}/**/*.html',
//    'src/*.html',
//    'src/{app,components}/**/*.html'
//  ]);
});
//
//gulp.task('serve:dist', ['build'], function () {
//  browserSyncInit('dist');
//});
//
//gulp.task('serve:e2e', ['wiredep', 'injector:js', 'injector:css'], function () {
//  browserSyncInit(['.tmp', 'src'], null, []);
//});
//
//gulp.task('serve:e2e-dist', ['build'], function () {
//  browserSyncInit('dist', null, []);
//});
