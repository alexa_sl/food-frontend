angular
  .module('photo')
  .factory('User', User);

User.$inject = ['$resource', 'AppConfig'];

function User($resource, AppConfig) {
  return $resource(
    AppConfig.apiPath + '/user/:userMethod',
    {

    },
    {
      update: {
        method: 'PUT'
      },
      login: {
        method: 'POST',
        params: {
          userMethod: 'signin'
        }
      },
      register: {
        method: 'POST',
        params: {
          userMethod: 'register'
        }
      }
    })
}
