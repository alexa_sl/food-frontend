angular
  .module('photo')
  .factory('Recipes', Recipes);

Recipes.$inject = ['$resource', 'AppConfig'];

function Recipes($resource, AppConfig) {
  return $resource(
    AppConfig.apiPath + '/recipes/:slug/',
    {slug: '@slug'},
    {
      update: {
        method: 'PUT'
      },
      getAll: {
        method: 'GET'
      },
      getRecipe: {
        method: 'GET',
        isArray: true
      },
      createRecipe: {
        method: 'POST'
      },
      removeRecipe: {
        method: 'DELETE'
      }
    })
}


