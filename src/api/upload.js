angular
  .module('photo')
  .factory('Uploads', Uploads);

Uploads.$inject = ['$resource', 'AppConfig'];

function Uploads($resource, AppConfig) {
    return $resource(
      AppConfig.apiPath + '/uploads/:id/',
      {id: '@id'},
      {
        update: {
          method: 'PUT'
        },
        getAll: {
          method: 'GET',
          isArray: true
        },
        createOne: {
          method: 'POST'
        }
      })
  }
