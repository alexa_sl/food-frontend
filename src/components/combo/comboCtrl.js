(function () {
    'use strict';

    angular
        .module('photo')
        .controller('ComboCtrl', ComboCtrl);

    ComboCtrl.$inject = ['recipesData', '$scope', 'lodash'];

    function ComboCtrl(recipesData, $scope, _) {
        var vm = this;

        $scope.allRecipes = recipesData.data;
        $scope.siutableRecipes = [];

        $scope.ingredients = [];

        angular.forEach($scope.allRecipes, function (item) {
            angular.forEach(item.ingredients, function (iItem) {
                if (iItem !== '') {
                    $scope.ingredients.push(iItem);
                }
            })
        });


        $scope.$on('ingredients:on', function (e, data) {
            console.log('on!!!', e, data);

            angular.forEach($scope.allRecipes, function (item) {
                angular.forEach(item.ingredients, function (iItem) {
                    if (iItem === data && (_.indexOf($scope.siutableRecipes, item) === -1)) {
                        $scope.siutableRecipes.push(item);
                    }
                })
            });
        });

        $scope.$on('ingredients:off', function (e, data) {
            console.log('off!', e, data);

            angular.forEach($scope.siutableRecipes, function (item) {
                angular.forEach(item.ingredients, function (iItem) {
                    if (iItem === data) {
                        _.remove($scope.siutableRecipes, item);
                        console.log($scope.siutableRecipes, item);
                    }
                })
            });
        });
    }
})();
