(function () {
  'use strict';

  angular
    .module('photo')
    .controller('IndexCtrl', IndexCtrl);

  IndexCtrl.$inject = ['$scope', '$window', '$state'];

  function IndexCtrl($scope, $window, $state) {

    $scope.goBack = function () {
      console.log('$window.history.length', $window.history.length);
      $window.history.length <= 2 ? $state.go('index') : $window.history.back();
    };

    $scope.date = new Date();
  }
})();
