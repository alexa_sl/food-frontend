angular
  .module('photo')
  .factory('TokenInterceptor', TokenInterceptor);

TokenInterceptor.$injector = ['$q', '$window', '$location', 'AuthService'];

function TokenInterceptor($q, $window, $location, AuthService) {
  return {
    request: function (config) {
      console.log('request int');
      config.headers = config.headers || {};
      if ($window.sessionStorage.token) {
        config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
      }
      return config;
    },

    requestError: function(rejection) {
      console.log('request int121');
      return $q.reject(rejection);
    },

    /* Set Authentication.isAuthenticated to true if 200 received */
    response: function (response) {


      //if (response != null && response.status == 200 && !!$window.sessionStorage.token.length && !AuthService.isLogged) {
      if (response != null && response.status == 200 &&  $window.sessionStorage.token && !AuthService.isLogged) {
        AuthService.isLogged = true;
      }

      return response || $q.when(response);
    },

    /* Revoke client authentication if 401 is received */
    responseError: function(rejection) {

      if (rejection != null && rejection.status === 401 && ($window.sessionStorage.token || AuthService.isLogged)) {
        delete $window.sessionStorage.token;
        AuthService.isLogged = false;
        $location.path("/admin");
      }

      return $q.reject(rejection);
    }
  };
}
