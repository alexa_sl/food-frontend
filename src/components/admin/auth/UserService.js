angular
  .module('photo')
  .factory('UserService', UserService);

UserService.$inject = ['User', '$q', '$state', '$window', 'AuthService', '$rootScope'];

function UserService(User, $q, $state, $window, AuthService, $rootScope) {

  return {
    login: function (username, password) {
      var deferred = $q.defer();

      User.login({
        username: username,
        password: password
      }, function (response) {
        $rootScope.$broadcast('auth:loggedIn');
        AuthService.isLogged = true;
        $window.sessionStorage.token = response.token;
        $state.reload();
        deferred.resolve(response);
      }, function (response) {
        var error = '';

        switch(response.status) {
          case 404:
            error = 'Пользователь с таким именем не найден';
            break;
          case 401:
            error = 'Пароли надо запоминать...';
            break;
          case 500:
            error = 'Sorry an error occurred';
            break;
          default:
            error = 'Sorry, we lost connection';
        }

        deferred.reject(error);
      });

      return deferred.promise;
    },


    register: function (username, password) {
      console.log('register serv', username, password);

      var deferred = $q.defer();

      User.register({
        username: username,
        password: password,
        passwordConfirmation: password
      }, function (response) {
        deferred.resolve(response);
      }, function (response) {
        console.log('errors', response);
        var errors = [];

        switch(response.status) {
          case 404:
            errors.push('Пользователь с таким именем не найден');
            break;
          case 401:
            if (response.data.length) {
              errors = response.data;
            } else {
              errors.push('Пароли надо запоминать...');
            }
            break;
          case 500:
            errors.push('Sorry an error occurred');
            break;
          default:
            errors.push('Sorry, we lost connection');
        }

        deferred.reject({
          errors: errors
        });
      });

      return deferred.promise;
    },

    isAuth: function(){
      console.log('is auth');
      return $rootScope.isAuth;
    }
  }
}
