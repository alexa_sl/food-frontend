(function () {
    'use strict';

    angular
        .module('photo')
        .controller('IngredientsCtrl', IngredientsCtrl);

    IngredientsCtrl.$inject = [ '$scope'];

    function IngredientsCtrl($scope) {
        $scope.isActive = false;

        $scope.chooseIngredient = function (ingredient) {
            $scope.isActive = !$scope.isActive;

            if ($scope.isActive) {
                $scope.$emit('ingredients:on', ingredient);
            } else {
                $scope.$emit('ingredients:off', ingredient);
            }
        };


    }
})();