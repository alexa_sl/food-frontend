(function () {
    'use strict';

    angular
        .module('photo')
        .directive('recipeItem', recipeItemDirective);

    function recipeItemDirective() {
        return {
            restrict: 'EA',
            replace: true,
            scope: {
              recipe: '='
            },
            templateUrl: '/views/components/recipes/recipeItem.html',
            link: function ($scope, $element, $attrs) {

            }
        };
    }
})();