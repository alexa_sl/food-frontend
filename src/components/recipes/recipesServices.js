(function () {
    'use strict';

    angular
        .module('photo')
        .service('RecipesService', RecipesService);

    RecipesService.$inject = ['$q', 'Recipes'];

    function RecipesService($q, Recipes) {
        return {

            createRecipte: function (title, content, category, tags, source) {
                var deferred = $q.defer();

                Recipes.createRecipe({
                    title: title,
                    description: content,
                    category: category,
                    ingredients: tags,
                    source: source
                }, function (response) {
                    console.log('create response', response);
                    deferred.resolve(response);
                }, function (error) {
                    console.log('create error', error);
                    deferred.reject(error);
                });

                return deferred.promise;
            },

            removeRecipe: function (slug) {
                var deferred = $q.defer();

                Recipes.removeRecipe({
                    slug: slug
                }, function (response) {
                    deferred.resolve(response);
                }, function (error) {
                    deferred.reject(error);
                });

                return deferred.promise;
            }
        }
    }
})();