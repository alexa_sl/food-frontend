(function () {
    'use strict';

    angular
        .module('photo')
        .controller('addRecipeCtrl', addRecipeCtrl);

    addRecipeCtrl.$inject = ['$scope', 'RecipesService', '$timeout', 'lodash'];

    function addRecipeCtrl($scope, RecipesService, $timeout, _) {
        $scope.isAdded = false;
        $scope.form = {
            title: '',
            description: '',
            categories: [
                {name: 'Первые блюда', slug: 'category-1'},
                {name: 'Вторые блюда', slug: 'category-2'},
                {name: 'Салаты', slug: 'category-3'},
                {name: 'Мясо, птица', slug: 'category-4'},
                {name: 'Рыба, морепродукты', slug: 'category-5'},
                {name: 'Десерты', slug: 'category-6'},
                {name: 'Консервация', slug: 'category-7'},
                {name: 'Другое', slug: 'category-8'}
            ],
            selectedCategory: '',
            source: '',
            tags: [''],
            error: '',
            success: 'Congratulations, you\'ve added question!'
        };

        $scope.addRecipe = function () {

            RecipesService.createRecipte($scope.form.title, $scope.form.content, $scope.form.selectedCategory.slug, $scope.form.tags, $scope.form.source).then(function(){
                    $scope.isAdded = true;
                    $timeout(function(){
                        $scope.isAdded = false;
                        $scope.form = {
                            title: '',
                            description: '',
                            categories: [
                                {name: 'Первые блюда', slug: 'category-1'},
                                {name: 'Вторые блюда', slug: 'category-2'},
                                {name: 'Салаты', slug: 'category-3'},
                                {name: 'Мясо, птица', slug: 'category-4'},
                                {name: 'Рыба, морепродукты', slug: 'category-5'},
                                {name: 'Десерты', slug: 'category-6'},
                                {name: 'Консервация', slug: 'category-7'},
                                {name: 'Другое', slug: 'category-8'}
                            ],
                            selectedCategory: '',
                            selectedAuthor: '',
                            source: '',
                            tags: ['']
                        };
                    }, 2000);
                },
                function(error) {
                    $scope.form.error = error;
                });
        };

        $scope.tags = [{
            id: 1
        }];

        $scope.addTagField = function(){
            $scope.tags.push({id: $scope.tags.length + 1});
        };

        $scope.removeTagField = function(chosen){
            _.remove($scope.tags, function (n) {return n.id == chosen});
        };
    }
})();