'use strict';

angular.module('photo', [
    'app.templates',
    'ngAnimate',
    'ngCookies',
    'ngTouch',
    'ngSanitize',
    'ngResource',
    'ui.router',
    'ui.bootstrap',
    'ngMap',
    'ipCookie',
    'ui.select',
    'ngLodash'
])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

        $stateProvider
            .state('global', {
                abstract: true
            })
            .state('index', {
                parent: 'global',
                url: '/',
                templateUrl: '/views/app/main.page/main.html',
                access: {requiredLogin: false}
            })
            .state('contacts', {
                parent: 'global',
                url: '/contacts',
                templateUrl: '/views/app/contacts.page/main.html',
                resolve: {
                    globalData: function () {
                        console.log('Contacts!');
                    }
                },
                access: {requiredLogin: false}
            })
            .state('add', {
                parent: 'global',
                url: '/add',
                templateUrl: '/views/app/main.addRecipe.html',
                controller: 'addRecipeCtrl',
                access: {requiredLogin: false}
            })
            .state('combo', {
                parent: 'global',
                url: '/combo',
                templateUrl: '/views/app/combo.page/main.combo.html',
                controller: 'ComboCtrl',
                resolve: {
                    recipesData: ['Recipes', '$stateParams', function (Recipes, $stateParams) {
                        console.log(1111);
                        return Recipes.getAll().$promise.then(function (response) {
                            console.log('albums response', response);
                            return response;
                        }, function () {
                            console.log('albums data error!');
                        });
                    }]
                },
                access: {requiredLogin: false}
            })
            .state('admin', {
                parent: 'global',
                url: '/admin/login',
                templateUrl: '/views/app/admin.page/main.admin.html',
                resolve: {
                    globalData: function () {
                        console.log('admin!');
                    }
                },
                access: {requiredLogin: false}
            })
            .state('404', {
                parent: 'global',
                url: '/404',
                template: '<h1>Not found:(</h1>',
                access: {requiredLogin: false}
            });

        $urlRouterProvider.otherwise('/');

        $httpProvider.interceptors.push('TokenInterceptor');
        $locationProvider.html5Mode(true).hashPrefix('!');
    })
    .run(function ($rootScope, $state, AuthService, $window) {
        $rootScope.$state = $state;
        console.log($rootScope);

        $rootScope.$on('auth:loggedIn', function () {
            AuthService.isLogged ? $rootScope.isAuth = true : $rootScope.isAuth = false;
            console.log('AuthService.isLogged', AuthService.isLogged);
        });

        $rootScope.$on('$stateChangeStart', function (event, nextRoute) {
            $rootScope.$emit('auth:loggedIn');
            $rootScope.loading = true;
            console.log('before', nextRoute.access.requiredLogin && !AuthService.isLogged);

            AuthGlobal();
            if (nextRoute.access.requiredLogin && !AuthService.isLogged) {
                event.preventDefault();
                $state.go('admin', {redirect: true});
            }
        });

        $rootScope.$on('$stateChangeSuccess', function (event, route) {
            $('html, body').animate({scrollTop: 0}, 400, 'easeOutCubic');
            $rootScope.loading = false;
            route.name === 'index' ? $rootScope.bodyClass = 'index-page' : $rootScope.bodyClass = '';

            if ($('.navbar-collapse.in').length) {
                $('.cmn-toggle-switch').click();
            }

        });

    })
;
