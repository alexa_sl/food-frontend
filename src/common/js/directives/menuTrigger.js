(function () {
  'use strict';

  angular
    .module('photo')
    .directive('menuTrigger', menuTriggerDirective);

  function menuTriggerDirective() {
    return {
      restrict: 'EA',
      replace: true,
      link: function ($scope, $element, $attrs) {
        var $collapse = $('.navbar-collapse');
        console.log($collapse);

        $collapse
          .on('show.bs.collapse', function () {
            console.log('show !!');
            $element.addClass('active');
          })
          .on('hide.bs.collapse', function () {
            console.log('hide !!');
            $element.removeClass('active');
          });
      }
    };
  }
})();
