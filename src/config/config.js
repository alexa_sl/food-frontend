(function () {
  var appUrlValue = 'http://178.62.229.183/api/v1';
  var apiUrlValue = 'http://localhost:1445/api/v1';
  var staticLocal = 'http://178.62.229.183';
  var staticLocal2 = 'http://localhost:1445';

  angular
    .module('photo')
    .constant('AppConfig', {
      apiPath: apiUrlValue,
      staticLocal: staticLocal,
      fileLimits: {
        images: {
          types: ['image/gif', 'image/jpeg', 'image/png', 'image/jpg']
        }
      }
    })
})();
